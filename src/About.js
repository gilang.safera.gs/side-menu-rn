import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";



export default class About extends Component {
    constructor(props) {
        super(props)
        this.state = {};
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}> Ini Halaman About </Text>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:  {
        alignItems: "center",
        justifyContent: "center",
        flex : 1,
        flexDirection : "column",
    },

    header: {
        fontWeight: "bold",
        fontSize: 18
    }
})