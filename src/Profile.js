import React, {Component} from "react";
import {Text,View, StyleSheet} from "react-native";

export default class Profile extends Component {

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.header}> Ini Halaman Profile </Text>
                <Text > Gilang Safera putra </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:  {
        alignItems: "center",
        justifyContent: "center",
        flex : 1,
        flexDirection : "column",
    },

    header: {
        fontWeight: "bold",
        fontSize: 18
    }
})