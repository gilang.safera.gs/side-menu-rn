import React,{ Component  } from "react";
import { View,Text, StyleSheet } from "react-native";



export default class Gilang extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.header}>
                    Gilang Safera Putra
                </Text>
                <Text>
                    Student salt {this.props.numBatch}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },

    header: {
        fontSize: 18,
        fontWeight: "bold"
    }
})