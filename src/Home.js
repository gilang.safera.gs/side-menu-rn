import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";



export default class Home extends Component {
    
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.header}>Ini Halaman Home</Text>
                <Text> Swipe layar kiri, untuk membuka Drawer</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:  {
        alignItems: "center",
        justifyContent: "center",
        flex : 1,
        flexDirection : "column",
    },

    header: {
        fontWeight: "bold",
        fontSize: 18
    }
})