import 'react-native-gesture-handler';
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import Home from './src/Home'
import Profile from './src/Profile'
import About from './src/About'
import Gilang from './src/Gilang'


const Drawer = createDrawerNavigator()


function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={Home}/>
        <Drawer.Screen name="Profile" component={Profile}/>
        <Drawer.Screen name="About" component={About}/>
        <Drawer.Screen name="Gilang" component={Gilang}/>
      </Drawer.Navigator>
    </NavigationContainer>
  )
}

export default App;
